# OpenAPI Specification Sandbox

Work in progress (pre-alpha).

Before you start: `npm install -g vitepress && npm install`

To develop: `vitepress dev`.

To build: `vitepress build`.

---

## License

Contents of the JSON files in the `/public` directory: [Apache 2.0](https://gerrit.wikimedia.org/r/plugins/gitiles/generated-data-platform/aqs/device-analytics/+/refs/heads/main/LICENSE).

Everything else: [MIT-0](https://opensource.org/license/mit-0).

## Dependencies

In [DEPENDENCIES.md](/DEPENDENCIES.md).