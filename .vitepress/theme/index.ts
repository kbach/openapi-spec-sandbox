 // https://vitepress.dev/guide/custom-theme
import { h } from 'vue'
import type { Theme } from 'vitepress'
import DefaultTheme from 'vitepress/theme-without-fonts'
import './style.css'

export default {
  extends: DefaultTheme,
  Layout: () => {
    return h(DefaultTheme.Layout, null, {
      // https://vitepress.dev/guide/extending-default-theme#layout-slots
    })
  },
  async enhanceApp({ app, router, siteData }) {
/*     if (!import.meta.env.SSR) {
      const rd = await import('rapidoc');
      const sc = await import('@scalar/api-reference');
      app.use(rd);
      app.use(sc);
    } */
  }
} satisfies Theme

/* import Layout from './Layout.vue'

export default {
  Layout,
  enhanceApp({ app, router, siteData }) {
    app.component('rapidoc')
  }
} */
