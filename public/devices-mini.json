{
    "schemes": [
        "http"
    ],
    "swagger": "2.0",
    "host": "wikimedia.org/api/rest_v1",
    "basePath": "/metrics/",
    "paths": {
        "/unique-devices/{project}/{access-site}/{granularity}/{start}/{end}": {
            "get": {
                "description": "Given a Wikimedia project and a date range, returns the number of unique devices that visited that wiki.",
                "produces": [
                    "application/json"
                ],
                "summary": "Get unique devices per project",
                "parameters": [
                    {
                        "type": "string",
                        "example": "en.wikipedia.org",
                        "description": "Domain of a Wikimedia project. For data grouped by project family, use all-[family]-projects (for example: all-wikipedia-projects)",
                        "name": "project",
                        "in": "path",
                        "required": true
                    },
                    {
                        "enum": [
                            "all-sites",
                            "desktop-site",
                            "mobile-site"
                        ],
                        "type": "string",
                        "example": "all-sites",
                        "description": "Method of access",
                        "name": "access-site",
                        "in": "path",
                        "required": true
                    },
                    {
                        "enum": [
                            "daily",
                            "monthly"
                        ],
                        "type": "string",
                        "example": "daily",
                        "description": "Time unit for response data",
                        "name": "granularity",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "example": "20220101",
                        "description": "First date to include, in YYYYMMDD format",
                        "name": "start",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "example": "20220108",
                        "description": "Last date to include, in YYYYMMDD format",
                        "name": "end",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/entities.UniqueDevicesResponse"
                        }
                    },
                    "400": {
                        "description": "Bad request",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "detail": {
                                    "type": "string"
                                },
                                "method": {
                                    "type": "string"
                                },
                                "status": {
                                    "type": "integer"
                                },
                                "title": {
                                    "type": "string"
                                },
                                "uri": {
                                    "type": "string"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not found",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "detail": {
                                    "type": "string"
                                },
                                "method": {
                                    "type": "string"
                                },
                                "status": {
                                    "type": "integer"
                                },
                                "title": {
                                    "type": "string"
                                },
                                "uri": {
                                    "type": "string"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal server error",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "detail": {
                                    "type": "string"
                                },
                                "method": {
                                    "type": "string"
                                },
                                "status": {
                                    "type": "integer"
                                },
                                "title": {
                                    "type": "string"
                                },
                                "uri": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "entities.UniqueDevices": {
            "type": "object",
            "properties": {
                "access-site": {
                    "description": "Method of access",
                    "type": "string",
                    "example": "all-sites"
                },
                "devices": {
                    "description": "Number of unique devices",
                    "type": "integer",
                    "example": 62614522
                },
                "granularity": {
                    "description": "Frequency of data",
                    "type": "string",
                    "example": "daily"
                },
                "offset": {
                    "description": "Number of devices with one visit or an unknown number of visits",
                    "type": "integer",
                    "example": 13127765
                },
                "project": {
                    "description": "Wikimedia project domain",
                    "type": "string",
                    "example": "en.wikipedia.org"
                },
                "timestamp": {
                    "description": "Timestamp in YYYYMMDD format",
                    "type": "string",
                    "example": "20220101"
                },
                "underestimate": {
                    "description": "Number of devices with two or more visits",
                    "type": "integer",
                    "example": 49486757
                }
            }
        },
        "entities.UniqueDevicesResponse": {
            "type": "object",
            "properties": {
                "items": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/entities.UniqueDevices"
                    }
                }
            }
        }
    }
}
