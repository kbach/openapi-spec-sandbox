---
outline: deep
---
<script setup>
import RapiDoc from '/src/components/RapiDoc.vue';
</script>
<RapiDoc specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/api-spec.json" />
