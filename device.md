---
outline: deep
---
<script setup>
import RapiDoc from '/src/components/RapiDoc.vue';
</script>

# Device analytics

Device analytics API provides you with ...

<RapiDoc specUrl="/devices.json" path="get /unique-devices"/>
