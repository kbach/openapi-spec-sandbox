import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "AQS Documentation Sandbox",
  description: "A VitePress Site",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' }
    ],

    sidebar: [
      {
        text: 'APIs',
        items: [
          { text: 'Device', link: '/device' },
          { text: 'Device (Scalar)', link: '/device-scalar' },
          { text: 'Device Mini (Scalar)', link: '/device-scalar-mini' },
          { text: 'Edit', link: '/edit' },
          { text: 'Editor', link: '/editor'}, 
          { text: 'Geo (Scalar)', link: '/geo'},
          { text: 'Media', link: '/media'},
          { text: 'Page', link: '/page'}
        ]
      }
    ],
  }
})
