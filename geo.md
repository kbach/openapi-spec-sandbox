---
layout: home
---
<script setup>
import Scalar from '/src/components/Scalar.vue';
</script>

<Scalar specUrl="https://wikimedia.org/api/rest_v1/metrics/editors/by-country/api-spec.json"/>