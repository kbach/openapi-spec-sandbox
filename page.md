---
outline: deep
---
<script setup>
import RapiDoc from '/src/components/RapiDoc.vue';
</script> 

## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.

## Spec

<RapiDoc specUrl="https://wikimedia.org/api/rest_v1/metrics/pageviews/api-spec.json" />