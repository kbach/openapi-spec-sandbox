# Dependency copyrights and licenses

## ESLint

Copyright OpenJS Foundation and other contributors, <www.openjsf.org>

[MIT](https://github.com/eslint/eslint/blob/main/LICENSE)

## RapiDoc

Copyright (c) 2022 Mrinmoy Majumdar

[MIT](https://github.com/rapi-doc/RapiDoc/blob/v9.3.4/LICENSE.txt)

## Scalar

Copyright (c) 2023-present Scalar

[MIT](https://github.com/scalar/scalar/blob/main/LICENSE)

## Typescript

[Apache 2.0](https://github.com/microsoft/TypeScript/blob/v5.4.4/LICENSE.txt)

## Vitepress

Copyright (c) 2019-present, Yuxi (Evan) You

[MIT](https://github.com/vuejs/vitepress/blob/v1.0.2/LICENSE)